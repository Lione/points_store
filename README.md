# 项目：积分商城
# PG：points_store

### 介绍
欢迎进入积分商城项目代码仓库，在这里需要各位同事完成某功能进度时就需要往`自己的仓库分支`提交代码，当进入功能整合阶段或测试阶段时就需要进行`分支的合并`，在这里建议大家使用`TortoiseGit`的仓库管理工具，废话不多说下面做一次操作演示。

### 仓库架构
主分支（master） [包含：frontend、backend目录]

开发分支（develop） [包含：frontend、backend目录]

前端个人分支（worker_1） [包含：frontend目录]

前端个人分支（worker_2） [包含：frontend目录]

后端个人分支（worker_3） [包含：backend目录]

后端个人分支（worker_4） [包含：backend目录]

### 使用流程

```
# 克隆仓库到本地开发环境
git clone git@gitee.com:Lione/points_store.git

# 创建并切换到个人分支，例如：我是Lione
git checkout -b Lione

# 创建相关目录，例如：我是前端，创建应用（points_store）
mkdir frontend // 创建前端目录
cd frontend // 进入前端目录
mkdir points_store // 创建应用（points_store）目录
cd points_store // 进入应用目录
npm init // 应用初始化

# 提交代码（建议用TortoiseGit图形化工具）
git add .
git commit -m '更新20200826，新增/更新/修复内容********'
git push origin Lione // 提交到个人分支

# 合并同事的代码
1. 拉取同事的分支
2. 合并
3. 测试
4. 没问题，提交；有问题，还原，处理，更新再合并
```

### 参与贡献

1.  后端：Lxs，Lgb
2.  前端：Lj，Zxj
